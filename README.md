# Hacks-Ai 2022 Kursk

## Solution from Sergeev Jury

### Clone Git repository

```
git clone git@gitlab.com:jurbanhost/hacks-ai-kursk.git
cd your-path/hacks-ai-kursk
```

### Install need packages

```
Windows:
"c:\Program Files\R\R-4.1.2\bin\Rscript.exe" install.R

Linux:
Rscript install.R

```

### Train and predict

```
Windows:
"c:\Program Files\R\R-4.1.2\bin\Rscript.exe" baseline_run_one_vs_rest_2_levels.R 

Linux:
Rscript baseline_run_one_vs_rest_2_levels.R 

```
### Get predicted test in

```
your-path/data/baseline_2_levels_x2_balance_rocauc_0.XXXX.csv 

```
